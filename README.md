# NAME

pdfunnel - Poor man's PDF manipulator

# VERSION

The version can be retrieved with option `--version`:

    $ pdfunnel --version

# USAGE

    pdfunnel [--help] [--man] [--usage] [--version]

    pdfunnel [--data|-d path]
             [--loglevel level]
             [--output|-o path]
             [--recipe|-r|-i path]
             [--set key1=value [--set|-s key2=value [...]]] 

# EXAMPLES

    # Some examples will help...
    pdfunnel -r mystuff.json -o result.pdf -s foo=bar

# DESCRIPTION

This program follows directives inside a ["**--recipe**"](#recipe) file to
generate a PDF output, sent to standard output or wherever ["**--output**"](#output) points to.

## Data

Some directives (notably, ["**add-text**"](#add-text)) can take
[Template::Perlish](https://metacpan.org/pod/Template%3A%3APerlish) templates as inputs, which can be expanded using a
provided data structure.

This is a hash of key/value pairs that can be initialized through
parameter ["**--data**"](#data) (which is assumed to contain a JSON-encoded
object) or through ["**--set**"](#set) to set an individual key/value pair.

## Recipe File

A recipe file contains a JSON-encoded array, where each element is a
_directive_ to perform a single action.

A _directive_ is a JSON-encoded object, with key `op` indicating in
its value the kind of directive.

Example:

    [
       {
          "op": "set-defaults",
          "font-family": "FreeSans.ttf",
          "font-size": 20
       },
       {
          "op": "add-page",
          "from": "pdf-input.pdf"
       },
       {
          "op": "add-text",
          "text": "Hello, World!",
          "x": 100, "y": 340
       },
       {
          "op": "add-text",
          "text-template": "Hello, World ([%= scalar localtime%])!",
          "x": 100, "y": 320
       },
       {
          "op": "add-text",
          "text-var": "foo.bar",
          "x": 100, "y": 300
       },
       {
          "op": "add-image",
          "path": "test.png",
          "x": 100, "y": 50,
          "width": 220, "height": 220
       },
       { "op": "log", "loglevel": "WARN", "text": "DONE!" }
    ]

The following subsections describe the availble directives that can be
put inside a recipe file.

### Directive: `add-image`

Add one image in a page. Available keys:

- `height`

    height of the image

- `page`

    page where the image is put. Can be an integer or the string `last`.
    Defaults to the last one.

- `path`

    path to the image file

- `width`

    width of the image

- `x`
- `y`

    lower-left corner coordinates

### Directive: `add-page`

Add one page to the PDF file. Available keys:

- `from`
- `from-path`

    path to a PDF file for copying the page. If missing, a new page is
    added.

- `from-page`

    page number for extracting a page from an input PDF file. Defaults to
    the last page.

- `page`

    position to put the page. Can be an integer or the string `last`.
    Defaults to the last one.

### Directive: `add-text`

Add a text line. Available keys:

- `font`
- `font-family`

    name or path to the font file for rendering the text.

- `font-size`

    size of the font for rendering the text.

- `page`

    page identifer for putting the text. Can be an integer or the string
    `last`. Defaults to the last one.

- `text`

    the content is taken verbatim from the value

- `text-template`

    the value is rendered through [Template::Perlish](https://metacpan.org/pod/Template%3A%3APerlish) to generate the
    content

- `text-var`

    the content is taken from the variable set in the value

- `x`
- `y`

    lower-left corner coordinates

### Directive: `log`

print a log on standard error. Allowed keys:

- `loglevel`

    the level at which the log will be emitted. Defaults to `INFO`.

- `text`

    the content is taken verbatim from the value

- `text-template`

    the value is rendered through [Template::Perlish](https://metacpan.org/pod/Template%3A%3APerlish) to generate the
    content

- `text-var`

    the content is taken from the variable set in the value

### Directive: `set-defaults`

set default values for other directives. Any key is supported.

# OPTIONS

- **--data**
- **-d**

        pdfunnel -d /path/to/data.json

    path to a JSON-encoded file with an object containing data used to
    expand \[Template::Perlish\]\[\] template fragments.

- **--help**

    print out some help and exit.

- **--loglevel**

        pdfunnel --loglevel DEBUG

    set the verbosity level.

- **--man**

    show the manual page for pdfunnel.

- **--output**
- **-o**

        pdfunnel -o /path/to/output.pdf

    path to the output file. Output defaults to standard output. A path
    equal to `-` means standard output.

- **--recipe**
- **-r**
- **-i**

        pdfunnel -i /path/to/recipe.json

    path to the recipe file (see ["Recipe File"](#recipe-file).

- **--set**
- **-s**

        pdfunnel -s key1=value1 -s key2=value2

    set a key/value pair in the data structure used for rendering
    [Template::Perlish](https://metacpan.org/pod/Template%3A%3APerlish) templates. Can be used multiple times to set many
    pairs.

    The key and value part are separated through a _separator_. Leading and
    trailing spaces are removed from both.

    The separator between the key and the value can be:

    - `=` or `:`

        the value is taken verbatim (after leading and trailing space trimming).

    - `#=` or `::`

        the value is interpreted as a base-64 encoded string and decoded as
        such.

- **--usage**

    show usage instructions.

- **--version**

    show version.

# DEPENDENCIES

- [PDF::Builder](https://metacpan.org/pod/PDF%3A%3ABuilder)
- [Image::PNG::Libpng](https://metacpan.org/pod/Image%3A%3APNG%3A%3ALibpng)
- [Template::Perlish](https://metacpan.org/pod/Template%3A%3APerlish)
- [Log::Log4perl::Tiny](https://metacpan.org/pod/Log%3A%3ALog4perl%3A%3ATiny)

# BUGS AND LIMITATIONS

Please report any bugs or feature requests through the repository at
[https://codeberg.org/polettix/pdfunnel](https://codeberg.org/polettix/pdfunnel).

# AUTHOR

Flavio Poletti

# LICENSE AND COPYRIGHT

Copyright 2022 by Flavio Poletti (flavio@polettix.it).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

or look for file `LICENSE` in this project's root directory.

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
